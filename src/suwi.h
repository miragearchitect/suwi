/*
	SUWI v0.0.0 (2024-05-20)

	Do this:
	#define IMPLEMENTATION_SUWI_H
	before you include this file in *one* C or C++ file to create the implementation.

	// i.e. it should look like this:
	#include ...
	#include ...
	#include ...
	#define IMPLEMENTATION_SUWI_H
	#include "suwi.h"

*/
#define DEBUG

#ifndef INCLUDE_SUWI_H
#define INCLUDE_SUWI_H

#include <SDL2/SDL.h>
#include <stdbool.h>

#define SUWI_ARRAY_GROW_SIZE 8

#ifndef SUWI_MALLOC
#include <stdlib.h>
#define SUWI_MALLOC(size) malloc(size)
#define SUWI_MEMMOVE(dest, src, size) memmove(dest, src, size)
#define SUWI_FREE(ptr) free(ptr)
#define SUWI_REALLOC(ptr, size) realloc(ptr, size)
#endif

bool suwiInit();

// a != 10

typedef enum {
	srcOriginal,
	srcAdd,
} PieceSource;

typedef struct {
	size_t start;
	size_t length;
	PieceSource source;
} Piece;

typedef struct {
	size_t originalSize;
	size_t addSize;
	size_t addFree;
	size_t piecesSize;
	size_t piecesFree;

	char *original;
	char *add;
	Piece *pieces;
} PieceTable;

size_t PieceTableStringLength(PieceTable *table);

void PieceTableInit(PieceTable *table);
bool PieceTableFree(PieceTable *table);
bool PieceTableLoad(PieceTable *table, char *filename);
size_t AddBufferAppend(PieceTable *table, const char *text);
bool pieceAdd(PieceTable *table, int start, int length, PieceSource source, int index);
bool pieceRemove(PieceTable *table, int index);
// bool piceTableEdit(PieceTable *table, char *text, size_t lenght, size_t position);
void PieceTablePrint(PieceTable *table);
char *PieceTableToString(PieceTable *table);

void pieceFind(PieceTable *table, size_t position, size_t *pieceIndex, size_t *offset);

bool insert(PieceTable *table, size_t position, char *text, size_t length);
bool delete(PieceTable *table, size_t position, size_t length);

#ifdef DEBUG
#define printD(fmt, ...) fprintf(stdout, "[%s:%d] " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#define error(fmt, ...)                                                                            \
	fprintf(stderr, "[%s:%d][ERROR] " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#else
#define printD(fmt, ...)                                                                           \
	do {                                                                                       \
	} while (0)
#define error(fmt, ...)                                                                            \
	do {                                                                                       \
	} while (0)
#endif

// Log macro
#define log(fmt, ...) fprintf(stderr, "[%s:%d] " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__)

// Memory error checking macro
#define memCheck(ptr)                                                                              \
	do {                                                                                       \
		if ((ptr) == NULL) {                                                               \
			fprintf(stderr, "[%s:%d] Memory allocation error\n", __FILE__, __LINE__);  \
			exit(EXIT_FAILURE);                                                        \
		}                                                                                  \
	} while (0)

#endif // INCLUDE_SUWI_H

#define IMPLEMENTATION_SUWI_H // TODO:Remove
#ifdef IMPLEMENTATION_SUWI_H

// bool suwiInit() {
// 	if (!SDL_Init(SDL_INIT_EVERYTHING))
// 		return false;
// 	return true;
// }

void PieceTableInit(PieceTable *table) {
	table->originalSize = 0;

	// Initialize add buffer
	table->add = NULL; // Initialize to NULL
	table->addSize = 0;
	table->addFree = 0;

	// Initialize pieces buffer
	table->pieces = NULL; // Initialize to NULL
	table->piecesSize = 0;
	table->piecesFree = 0;
}

bool PieceTableFree(PieceTable *table) {
	if (table == NULL)
		return true;

	// Free original buffer
	SUWI_FREE(table->original);
	table->originalSize = 0;

	// Free add buffer
	SUWI_FREE(table->add);
	table->addSize = 0;
	table->addFree = 0;

	// Free pieces buffer
	SUWI_FREE(table->pieces);
	table->piecesSize = 0;
	table->piecesFree = 0;

	return true;
}

size_t PieceTableStringLength(PieceTable *table) {
	size_t totalLength = 0;
	for (size_t pieceIndex = 0; pieceIndex < table->piecesSize; pieceIndex++) {
		totalLength += table->pieces[pieceIndex].length;
	}
	return totalLength;
}

size_t AddBufferAppend(PieceTable *table, const char *text) {
	size_t textLength = strlen(text);

	// Check if reallocation is needed
	if (table->addSize + textLength + 1 > table->addFree) {
		// Calculate new size for the add buffer
		size_t newSize = (table->addSize + textLength + 1) * 2; // Double the size

		// Reallocate memory for the add buffer
		char *newAdd = (char *)SUWI_REALLOC(table->add, newSize * sizeof(char));
		memCheck(newAdd);

		// Update the add buffer and its properties
		table->add = newAdd;
		table->addFree = newSize - table->addSize;
	}

	// Copy text to the add buffer
	strcpy(table->add + table->addSize, text);

	// Update add buffer size and free space
	size_t startIndex = table->addSize;
	table->addSize += textLength;
	table->addFree -= textLength;

	printD("Added to add buffer '%s' -> '%s'\n", text, table->add);

	return (size_t)startIndex;
}

bool pieceAdd(PieceTable *table, int start, int length, PieceSource source, int index) {
	if (length <= 0) {
		printD("ZERO LENGHT Piece@%d start=%d length=%d source=%s (Pieces=%zu)", index,
		       start, length, source == srcOriginal ? "Original" : "Add",
		       table->piecesSize);
		return false;
	}
	printD("Adding Piece@%d start=%d length=%d source=%s (Pieces=%zu)", index, start, length,
	       source == srcOriginal ? "Original" : "Add", table->piecesSize);

	// Increase the pieces array size
	table->pieces =
	    (Piece *)SUWI_REALLOC(table->pieces, (table->piecesSize + 1) * sizeof(Piece));
	memCheck(table->pieces);

	if (index <= -1) {
		// Calculate the index for the new piece
		index = table->piecesSize;
	} else {
		// shift elements after index.
		SUWI_MEMMOVE(&table->pieces[index + 1], &table->pieces[index],
			     (table->piecesSize - index) * sizeof(Piece));
	}

	// Add the new piece
	table->pieces[index].start = start;
	table->pieces[index].length = length;
	table->pieces[index].source = source;
	table->piecesSize++;

	return true;
}

void pieceFind(PieceTable *table, size_t position, size_t *pieceIndex, size_t *offset) {
	*offset = position;
	*pieceIndex = 0;
	while (*pieceIndex < table->piecesSize) {
		if (*offset <= table->pieces[*pieceIndex].length) {
			break;
		}
		*offset -= table->pieces[*pieceIndex].length;
		(*pieceIndex)++;
	}
}

bool insert(PieceTable *table, size_t position, char *text, size_t length) {
	if (!table || !text || length <= 0) {
		return false;
	}

	// Add text to the add buffer
	size_t startIndex = AddBufferAppend(table, text);

	size_t pieceIndex = 0;
	size_t remainingOffset = position;
	pieceFind(table, position, &pieceIndex, &remainingOffset);
	// while (pieceIndex < table->piecesSize) {
	// 	if (remainingOffset <= table->pieces[pieceIndex].length) {
	// 		break;
	// 	}
	// 	remainingOffset -= table->pieces[pieceIndex].length;
	// 	pieceIndex++;
	// }

	if (pieceIndex == table->piecesSize && remainingOffset != 0) {
		return false;
	}

	// Insert at start and end
	if (remainingOffset == 0 || pieceIndex == table->piecesSize) {
		pieceAdd(table, startIndex, length, srcAdd, 0);
	} else {
		// Merge pieces.
		if (table->pieces[pieceIndex].source == srcAdd) {
			table->pieces[pieceIndex].length += length;
			return true;
		}
		// insert in the middle
		size_t oldLength = table->pieces[pieceIndex].length;

		// Update the first piece aka. split it
		table->pieces[pieceIndex].length = remainingOffset;

		// Insert
		pieceAdd(table, startIndex, length, srcAdd, pieceIndex + 1);

		// The remaining piece
		if ((oldLength - remainingOffset) > 0) {
			pieceAdd(table, table->pieces[pieceIndex].start + remainingOffset,
				 oldLength - remainingOffset, table->pieces[pieceIndex].source,
				 pieceIndex + 2);
		}
	}

	return true;
}

bool delete(PieceTable *table, size_t position, size_t length) {
	if (!table) {
		return false;
	}
	// Locate piece
	size_t pieceIndex;
	size_t offset;
	pieceFind(table, position, &pieceIndex, &offset);
	printD("\t{DELETE position:%zu length:%zu offset:%zu piece index:%zu}\n", position, length,
	       offset, pieceIndex);

	// If deletion covers an entire piece, just remove it.
	if (offset == 0 && length == table->pieces[pieceIndex].length) {
		pieceRemove(table, pieceIndex);
		printD("Deleting piece %zu with length %zu\n", pieceIndex,
		       table->pieces[pieceIndex].length);
		printD("|- offset: %zu  length: %zu state:%d\n", offset, length,
		       (length == table->pieces[pieceIndex].length));
	} else {
		if (offset > 0) {
			// Split piece
			size_t oldLength = table->pieces[pieceIndex].length;
			table->pieces[pieceIndex].length = offset;

			// The remaining piece
			size_t difference = offset - length;
			size_t sum = offset + length;

			printD("Deleted:\n\t(old length=%zu)\n\t(difference=%zu)\n\t(sum=%zu)",
			       oldLength, difference, sum);
			if ((oldLength - difference) > 0) {
				pieceAdd(table, table->pieces[pieceIndex].start + offset + length,
					 oldLength - offset - length,
					 table->pieces[pieceIndex].source, pieceIndex + 1);
			}
		} else {
			table->pieces[pieceIndex].start += length;
		}
	}

	return true;
}

// bool undo() {}
// bool redo() {}

bool pieceRemove(PieceTable *table, int index) {
	if (index < 0 || index >= (int)table->piecesSize) {
		error("Invalid Piece index %d\n", index);
		return false;
	}

	// Shift elements after the removed piece one index backward
	for (size_t i = index; i < table->piecesSize - 1; ++i) {
		table->pieces[i] = table->pieces[i + 1];
	}

	// Decrement the number of pieces
	table->piecesSize--;

	// Increment the number of free pieces
	table->piecesFree++;

	printD("Removed piece %d\n", index);

	return true;
}

void PieceTablePrint(PieceTable *table) {
	printf("----------[PieceTable]----------\n");
	printf("Original Buffer:\n\t\"%s\"\n", table->original);
	printf("Add Buffer:\n\t\"%s\"\n", table->add);
	printf("Pieces(%ld):\n\n", table->piecesSize);
	if (table->piecesSize <= 0) {
		printf("No Pieces\n");
		return; // Return after printing the message
	}
	for (size_t i = 0; i < table->piecesSize; ++i) {
		char *source = table->pieces[i].source == srcOriginal ? "Original" : "Add";
		printf("\t[Piece  start=%zu  length=%zu  source=%s]\n", table->pieces[i].start,
		       table->pieces[i].length, source);
	}
	printf("Final:\t\"%s\"\n", PieceTableToString(table));
}

bool PieceTableLoad(PieceTable *table, char *filename) {
	FILE *file = fopen(filename, "r");
	if (file == NULL) {
		error("Error opening file %s\n", filename);
		return false;
	}

	// Find file size
	fseek(file, 0, SEEK_END);
	table->originalSize = ftell(file);
	rewind(file);

	// Allocate buffer for original
	table->original = (char *)SUWI_MALLOC((table->originalSize + 1) * sizeof(char));
	if (table->original == NULL) {
		error("Memory allocation error\n");
		fclose(file);
		return false;
	}

	// Read file to buffer
	size_t bytesRead = fread(table->original, sizeof(char), table->originalSize, file);
	if (bytesRead != table->originalSize) {
		error("Error reading file\n");
		fclose(file);
		SUWI_FREE(table->original);
		return false;
	}

	table->original[table->originalSize] = '\0';

	// Add the original Piece
	pieceAdd(table, 0, table->originalSize, srcOriginal, 0);

	// Print debug info
	printD("Original Size: %zu\n", table->originalSize);
	printD("Pieces: %ld\n", table->piecesSize);

	fclose(file);
	return true;
}

char *PieceTableToString(PieceTable *table) {
	// Calculate total length of the final string
	size_t totalLength = PieceTableStringLength(table);

	// Allocate memory for the final string
	char *string = (char *)malloc((totalLength + 1) * sizeof(char)); // +1 for null terminator
	if (string == NULL) {
		error("Memory allocation error\n");
		exit(EXIT_FAILURE);
	}

	size_t offset = 0;
	// Iterate over each piece and concatenate its text span to the final string
	for (size_t i = 0; i < table->piecesSize; i++) {
		PieceSource source = table->pieces[i].source;
		int start = table->pieces[i].start;
		int length = table->pieces[i].length;

		// Retrieve the buffer based on source
		char *buffer = (source == srcOriginal) ? table->original : table->add;

		// Check if adding this piece will overflow the buffer
		if (offset + length > totalLength) {
			error("Buffer overflow while concatenating text spans\n");
			free(string);
			exit(EXIT_FAILURE);
		}

		// Copy the text span to the final string directly
		memcpy(string + offset, buffer + start, length);
		offset += length;
	}

	// Null terminate the final string
	string[offset] = '\0';

	return string;
}

#endif // IMPLEMENTATION_SUWI_H
