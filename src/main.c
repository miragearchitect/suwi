#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdbool.h>
#include <stdlib.h> // for dynamic memory allocation
#include <string.h> // for string manipulation

#define IMPLEMENTATION_SUWI_H
#include "suwi.h"

// Texture caching
SDL_Texture *texture = NULL;
int version = 0;
int renderedVersion = 0;

// Render the text from the PieceTable
void renderText(
    SDL_Renderer *renderer, TTF_Font *font, PieceTable *table, int scrollX, int scrollY) {

	// Get the concatenated string from the PieceTable
	char *text = PieceTableToString(table);

	// Set text color
	SDL_Color color = {255, 255, 255, 255};

	if (texture == NULL || version != renderedVersion) {
		// Clean up.
		if (texture != NULL) {
			SDL_DestroyTexture(texture);
			texture = NULL; // Reset to NULL after destruction
		}
		// Render the text
		SDL_Surface *surface = TTF_RenderText_Blended_Wrapped(font, text, color, 800);
		// SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surface);
		texture = SDL_CreateTextureFromSurface(renderer, surface);

		// Get texture dimensions
		int texW = 0;
		int texH = 0;
		SDL_QueryTexture(texture, NULL, NULL, &texW, &texH);

		// Set rendering destination
		SDL_Rect dstrect = {0 - scrollX, 0 - scrollY, texW, texH};

		// Copy texture to renderer
		SDL_RenderCopy(renderer, texture, NULL, &dstrect);

		// Clean up
		SDL_FreeSurface(surface);

		// Update version
		renderedVersion = version;
	} else {
		// Get texture dimensions
		int texW = 0;
		int texH = 0;
		SDL_QueryTexture(texture, NULL, NULL, &texW, &texH);

		// Set rendering destination
		SDL_Rect dstrect = {0 - scrollX, 0 - scrollY, texW, texH};

		// Copy texture to renderer
		SDL_RenderCopy(renderer, texture, NULL, &dstrect);
	}

	// Free the string allocated by PieceTableToString
	SUWI_FREE(text);
}

void saveToFile(const char *filename, PieceTable *table) {
	// Open the file for writing
	FILE *file = fopen(filename, "w");
	if (file == NULL) {
		printf("Error opening file for writing.\n");
		return;
	}

	// Get the text from the PieceTable
	char *text = PieceTableToString(table);

	// Write the text to the file
	if (fprintf(file, "%s", text) < 0) {
		printf("Error writing to file.\n");
		fclose(file);
		return;
	}

	// Close the file
	fclose(file);

	// Free the dynamically allocated memory
	SUWI_FREE(text);

	printf("File saved successfully.\n");
}

int main(int argc, char **argv) {

	int fontSize = 16;
	int cursorWidth = 2;

	bool quit = false;
	SDL_Event event;
	SDL_Init(SDL_INIT_EVERYTHING);
	TTF_Init();

	SDL_Window *win = SDL_CreateWindow("suwi", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
					   800, 600, SDL_WINDOW_RESIZABLE);

	SDL_Renderer *renderer = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);
	TTF_Font *font = TTF_OpenFont("fonts/JuliaMono-ttf/JuliaMono-Regular.ttf", fontSize);
	// Piece table
	PieceTable table;
	PieceTableInit(&table);

	if (argc > 1) {
		PieceTableLoad(&table, argv[1]);
	}

	// Pice table text
	char *text;
	// int textLength = 0;

	// Cursor
	int cursorPosition = 0;
	// Scrolling
	int scrollX = 0;
	int scrollY = 0;

	while (!quit) {
		text = PieceTableToString(&table);
		int textLength = strlen(text);

		SDL_WaitEvent(&event);

		switch (event.type) {
		case SDL_QUIT:
			quit = true;
			break;
		// Cursor
		case SDL_KEYDOWN:
			switch (event.key.keysym.sym) {
			case SDLK_RETURN:
				printf("Return\n");
				insert(&table, cursorPosition, "\n", strlen("\n"));
				cursorPosition++;
				version++;
				break;
			case SDLK_BACKSPACE:
				// Check if cursor position is not at the beginning of the text
				if (cursorPosition > 0) {
					// Delete the character before the cursor
					delete (&table, cursorPosition - 1, 1);
					// printf("BACKSPACE %d\n", cursorPosition - 1);
					// Move the cursor one position to the left
					cursorPosition--;
					version++;
				}
				break;
			case SDLK_LEFT:
				if (cursorPosition > 0)
					cursorPosition--;
				// printf("| at: %d\n", cursorPosition);
				break;
			case SDLK_RIGHT:
				if (cursorPosition < textLength)
					cursorPosition++;
				// printf("| at: %d\n", cursorPosition);

				break;
			case SDLK_UP:
				// Move cursor up by finding the beginning of the previous line
				if (cursorPosition > 0) {
					cursorPosition--;
					while (cursorPosition > 0 &&
					       text[cursorPosition - 1] != '\n') {
						cursorPosition--;
					}
				}
				break;
			case SDLK_DOWN:
				// Move cursor down by finding the end of the current line
				while (cursorPosition < textLength &&
				       text[cursorPosition] != '\n') {
					cursorPosition++;
				}
				if (cursorPosition < textLength) {
					cursorPosition++;
					while (cursorPosition < textLength &&
					       text[cursorPosition] != '\n') {
						cursorPosition++;
					}
				}
				break;
			case SDLK_s:
				if (event.key.keysym.mod & KMOD_CTRL) {
					char *filename;
					if (argc > 1) {
						filename = argv[1];
					} else {
						filename = "file.txt";
					}
					saveToFile(filename, &table);
					printf("Saved file \"%s\"!\n", filename);
					version++;
				}
				break;
			default:
				break;
			}
			break;
		case SDL_MOUSEWHEEL:
			if (event.wheel.y > 0) {
				scrollY += fontSize; // scroll up
			} else if (event.wheel.y < 0) {
				scrollY -= fontSize; // scroll down
			}
			break;
		case SDL_TEXTINPUT: {
			size_t inputLength = strlen(event.text.text);
			printf("Text length: %d, Cursor position: %d\n", textLength,
			       cursorPosition);

			insert(&table, cursorPosition, event.text.text, strlen(event.text.text));

			// Update cursor position
			cursorPosition += inputLength;

			version++;
		} break;

		default:
			break;
		}

		// Get window size
		int windowWidth, windowHeight = 0;
		SDL_GetWindowSize(win, &windowWidth, &windowHeight);

		// Get font line height
		int fontLineHeight = TTF_FontLineSkip(font);

		// Calculate cursor position
		int cursorX = 0;
		int cursorY = 0;
		for (int i = 0; i < cursorPosition; ++i) {
			char c = text[i];
			int charWidth = 0;
			if (c == '\n') {
				cursorY += fontLineHeight; // Move to the next line
				cursorX = 0;		   // Reset X position to start of line
			} else {
				TTF_GlyphMetrics(font, c, NULL, NULL, NULL, NULL, &charWidth);
				cursorX += charWidth;
			}
		}

		// Calculate font height
		int fontHeight = TTF_FontHeight(font);
		// Check if cursor is off-screen and adjust scroll offsets accordingly
		if (cursorY < -scrollY) {
			scrollY = -cursorY;
		} else if (cursorY + fontHeight > windowHeight - scrollY) {
			scrollY = cursorY + fontHeight - windowHeight;
		}

		SDL_RenderClear(renderer);

		// Render the text from the PieceTable
		renderText(renderer, font, &table, scrollX, scrollY);

		// Render cursor
		SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
		SDL_Rect cursor = {cursorX, cursorY, cursorWidth, fontSize};
		SDL_RenderFillRect(renderer, &cursor);

		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
		SDL_RenderPresent(renderer);
	}
	PieceTablePrint(&table);
	printf("Text: %s\n", text);
	free(text); // Free dynamically allocated memory
	PieceTableFree(&table);
	TTF_CloseFont(font);

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(win);
	TTF_Quit();
	SDL_Quit();
	return 0;
}
