#https://gist.github.com/makerj/e720ea95e99d2ef35189887b228e1596
CC=gcc
#CFLAGS=-Wall -Wextra -Werror -O3 -std=c99 -g
CFLAGS=-Wall -Wextra  -O3 -std=c99 -g 
# CFLAGS=-Wall -Wextra  -O3 -std=c99 -g# =$(shell sdl2-config --cflags sdl2)  $(shell sdl2-config --cflags SDL2_ttf)

SRCS=$(wildcard src/*.c)
OBJS=$(SRCS:%.c=%.o)
LIBS=$(shell pkg-config --cflags --libs sdl2) $(shell pkg-config --cflags --libs SDL2_ttf)

OBJDIR=build/obj
BINDIR=build

.PHONY: all clean mktree

all: editor
	

%.o: %.c
	$(CC) $(CFLAGS) $(LIBS) $(shell pkg-config --cflags sdl2) -c -o $(OBJDIR)/$(notdir $@) $^

editor: mktree $(OBJS)
	$(CC) $(CFLAGS) $(LIBS) $(OBJDIR)/*.o -o $(BINDIR)/editor

mktree:
	mkdir -p $(OBJDIR)

clean:
	rm -rf build

run: clean editor
	./$(BINDIR)/editor text.txt
