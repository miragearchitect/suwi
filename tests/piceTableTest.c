#include "../src/suwi.h"
#include "greatest.h"

PieceTable table;

TEST insetAtStartOnce(void) {
	char *string = "Hello";
	insert(&table, 0, string, strlen(string));
	ASSERT_STR_EQ(table.add + 2, string);
	PASS();
}

TEST insetMiddle(void) {
	const char *string = ", ";
	insert(&table, 0, string, strlen(string));
	ASSERT_STR_EQ(table.add, string);
	PASS();
}

TEST deleteTest(void) {
	delete (&table, 5, 1);
	ASSERT_STR_EQ(PieceTableToString(&table), "ello ,World!");
	PASS();
}

GREATEST_MAIN_DEFS();
int main(int argc, char **argv) {
	GREATEST_MAIN_BEGIN(); /* command-line options, initialization. */

	PieceTableInit(&table);
	PieceTableLoad(&table, "test.txt");

	/* Individual tests can be run directly in main, outside of suites. */
	RUN_TEST(insetMiddle);
	RUN_TEST(insetAtStartOnce);
	PieceTablePrint(&table);

	RUN_TEST(deleteTest);

	/* Tests can also be gathered into test suites. */
	// RUN_SUITE(the_suite);
	PieceTablePrint(&table);
	GREATEST_MAIN_END(); /* display results */
}