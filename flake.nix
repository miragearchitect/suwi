{
  description = "A basic flake with a shell";
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

  outputs = { nixpkgs, ... }:
      let
        pkgs = nixpkgs.legacyPackages."x86_64-linux";
      in
      {
        devShells.x86_64-linux.default = pkgs.mkShell {
					packages = with pkgs; [
          	SDL2
          	SDL2_ttf
          	SDL2_sound
          	SDL2_image
          	SDL2_Pango
						pkg-config
						clang-tools
						gnumake
						valgrind
						gdb
          ];

					

          # packages = with pkgs; [ 
          # 	bashInteractive
					# 	clang-tools
					# 	cmake
					# 	codespell
					# 	conan
					# 	doxygen
					# 	gcc
					# 	clang
					# 	gnumake
					# 	bc
					# 	pkg-config
					# 	binutils
          # ];
        };
      };
}
