# Suwi
> Edit code with a *cute* text editor.

**Eun Tests:** `cc -g -Wall -pedantic tests/piceTableTest.c -o pt && ./pt`

## Todo
- [X] Fix deleting
- [X] Fix Insertion
- [ ] Fix performance
- [ ] Fix scrolling

- [X] Add a cursor
- [X] Make the cursor move by char width
- [X] Allow text input
- [X] Implement the Pice Table
- [ ] Use mouse for movement
- [X] Allow file saving
- [ ] Display line numbers


